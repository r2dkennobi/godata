package stack

// Stack is the root node for a stack
type Stack struct {
	Root []interface{}
	size int
}

// New creates a new stack instance. Pre-allocate memory for the stack.
func New() *Stack {
	return &Stack{Root: make([]interface{}, 20), size: 0}
}

// IsEmpty returns true if stack is empty
func (r *Stack) IsEmpty() bool {
	return r.size == 0
}

// Size returns the size of the stack
func (r *Stack) Size() int {
	return r.size
}

// Push pushes a node into the stack
func (r *Stack) Push(val interface{}) {
	if r.size == len(r.Root) {
		newArr := make([]interface{}, len(r.Root)+20)
		copy(newArr, r.Root)
		r.Root = newArr
	}
	r.Root[r.size] = val
	r.size++
}

// Peek reveals the data on top of the stack
func (r *Stack) Peek() interface{} {
	if r.IsEmpty() {
		return nil
	}
	return r.Root[r.size-1]
}

// Pop removes and returns the item on top of the stack
func (r *Stack) Pop() (val interface{}) {
	popped := r.Peek()
	if !r.IsEmpty() {
		r.size--
	}
	return popped
}
