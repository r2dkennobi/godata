package stack

import "testing"

func TestDefault(t *testing.T) {
	stack := new(Stack)
	if !stack.IsEmpty() {
		t.Error("Default stack is not empty!")
	}
}

func TestPush(t *testing.T) {
	stack := new(Stack)
	stack.Push("first")
	if stack.IsEmpty() {
		t.Error("Node did not get inserted!")
	}
	if stack.Peek() != "first" {
		t.Error("Didn't insert the correct element!?")
	}
}

func TestPop(t *testing.T) {
	stack := new(Stack)
	stack.Push("first")
	if stack.IsEmpty() {
		t.Error("Node did not get inserted!")
	}
	if stack.Peek() != "first" {
		t.Error("Didn't insert the correct element!?")
	}
	popped := stack.Pop()
	if popped != "first" {
		t.Error("Didn't pop the correct element!?")
	}
	if !stack.IsEmpty() {
		t.Error("Node didn't get popped")
	}
}

func TestEmptyPop(t *testing.T) {
	stack := new(Stack)
	popped := stack.Pop()
	if popped != nil {
		t.Error("Node couldn't have had data...")
	}
}

func TestMultipleEmptyPop(t *testing.T) {
	stack := new(Stack)
	popped := stack.Pop()
	for i := 0; i < 100; i++ {
		popped = stack.Pop()
	}
	if popped != nil {
		t.Error("Node couldn't have had data...")
	}
}

func TestMultiplePush(t *testing.T) {
	stack := new(Stack)
	for i := 0; i < 100; i++ {
		stack.Push(i)
	}
	if stack.Size() != 100 {
		t.Error("Node doesn't have the required number of elements!")
	}
}
